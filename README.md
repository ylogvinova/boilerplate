# Hey this is a simple boilerplate

`p.s. if you run it before, use up --build --force-recreate`

## FOR LOCAL
``
docker-compose -f docker-compose.local.yml build
docker-compose -f docker-compose.local.yml up
``
1. First make sure you have `npm install -g sequelize sequelize-cli`
2. To start working in DEV mode you need to run `docker-compose up` it will create server and db
  * In DEV mode we have hot reload, so you don't need to run it again
3. For prod mode `need to prepare DOCKER` but in package.json we have `build` and `start`
4. You can use `.env.copy` for now, just remove `.copy`
  
## FOR TEST 
Please uncomment .env DB_TEST_HOST and NODE_ENV
```
docker-compose -f docker-compose.test.yml build
docker-compose -f docker-compose.test.yml up
```

## FOR PROD 
```
docker-compose -f docker-compose.test.yml build
docker-compose -f docker-compose.test.yml up
```
  
### DB documentation
see documentation [here](db/README.md)

### Project documentation
see documentation [here](src/README.md)

### Config documentation
see documentation [here](config/README.md)
