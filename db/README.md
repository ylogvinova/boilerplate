# Work with DB

1. `npm run model:new` - will create a new model, use it with `--name`
2. `npm run migration:new` - will create a new migration, use it with `--name`
2. `npm run seeder:new`- will create a new seed, use it with `--name`

#### LOGIN
-  req: 
```
{
  "firstName": "first",
  "lastName": "last",
  "email": "email@email.com",
  "password": "test_pass"
}
```
- res: `{
            "loggedToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWJqZWN0Ijp7ImlkIjoyLCJmaXJzdE5hbWUiOiJmaXJzdCIsImxhc3ROYW1lIjoibGFzdCIsImVtYWlsIjoiZW1haWxAZW1haWwuY29tIiwicGFzc3dvcmQiOiIkMmIkMTAkY1pQdEY5My5TYS41U25TeVVQVVUudWZxb3BRVnpsN2dRNVguVTZVcHFwY0lEbjgyZUI2Z2kiLCJjcmVhdGVkQXQiOiIyMDIxLTA5LTA2VDIxOjI0OjIyLjAzMFoiLCJ1cGRhdGVkQXQiOiIyMDIxLTA5LTA2VDIxOjI0OjIyLjAzMFoifSwiZGF0YSI6eyJ1c2VyX2lkIjoyfSwiZXhwIjoxNjMwOTY3MTAxLCJpYXQiOjE2MzA5NjM1MDF9.iySRT6Zmaw8Wx-LuJ0eQtLs1keq22lIGqqB4STZjuTg"
        }`



#### REGISTER
- req:
```
{
  "firstName": "first",
  "lastName": "last",
  "email": "email@email.com",
  "password": "test_pass"
}
```
res: `register success`

#### TO go somewhere
put `authorization`: `..loggedToken..` to headers and feel ~~free~~
