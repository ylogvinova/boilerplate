module.exports = {
  'development': {
    'username': process.env.POSTGRES_USER,
    'password': process.env.POSTGRES_PASSWORD,
    'database': process.env.POSTGRES_DB,
    'host': process.env.DB_HOST,
    'port': process.env.DB_PORT,
    'dialect': 'postgres'
  },
  'stage': {
    'username': process.env.POSTGRES_USER,
    'password': process.env.POSTGRES_PASSWORD,
    'database': process.env.POSTGRES_DB,
    'host': process.env.DB_HOST,
    'dialect': 'postgres'
  },
  'production': {
    'username': process.env.POSTGRES_USER,
    'password': process.env.POSTGRES_PASSWORD,
    'database': process.env.POSTGRES_DB,
    'host': process.env.DB_HOST,
    'dialect': 'postgres'
  },
  'test': {
    'username': process.env.POSTGRES_USER,
    'password': process.env.POSTGRES_PASSWORD,
    'database': process.env.POSTGRES_DB,
    'host': process.env.DB_HOST_TEST,
    'dialect': 'postgres',
    "logging": false
  },
};
