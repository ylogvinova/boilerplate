import express from 'express';
const asyncify = require('express-asyncify')
const router = asyncify(express.Router());
import { registerUsers, loginUsers, getUsers } from './controller'


router.get('/', async (req: express.Request, res: express.Response) => {
  const users = await getUsers()
  res.render('users', {users: JSON.stringify(users)})
});

router.post('/login', async (req: express.Request, res: express.Response) => {
   const loggedToken = await loginUsers(req.body.email, req.body.password)
   res.json({
     loggedToken
   });
});

router.post('/register', async (req: express.Request, res: express.Response) => {
    await registerUsers(req.body)
    res.send('register success')
});

export default router;
