import User from './interface'
import {getUser, registerUser, loginUser} from './service'

export async function getUsers() {
  return await getUser()
}

export async function registerUsers(user: User) {
  return await registerUser(user)
}

export async function loginUsers(email: string, password: string) {
 return await loginUser(email, password)
}

export async function userTest(number: number) {
  return number * 3
}
