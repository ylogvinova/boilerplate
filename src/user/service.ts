import bcrypt from 'bcrypt';
import jsonwebtoken from 'jsonwebtoken';
const { secret } = require('../../config/server');
const db = require('../../db/models');
import User from './interface';

export async function getUser() {
 return await db.user.findAll()
}

export async function registerUser(user: User) {
  await db.user.create({
    firstName: user.firstName,
    lastName: user.lastName,
    email: user.email,
    password: user.password
  });
}

export async function loginUser(email: string, password: string) {
  const user = await db.user.findOne({
    where: {
      email: email
    }
  })

  const passwordCheck = await bcrypt.compare(password, user.dataValues.password)

  if (passwordCheck) {
    return jsonwebtoken.sign({
      subject: user.dataValues,
      data: {
        user_id: user.id
      },
      exp: Math.floor(Date.now()/1000) + (60 * 60)
    }, secret)
  }
}
