1. /static - use for static data
2. /middlewares - use for helpful functions
   * errors - handle errors
   * logger - create a file with current date and log there info (in /static folder)
   * verification - checks authorization header, and if token is not valid -> throw the message,
   otherwise will allow go to next 
3. /user - all methods for user
4. /views - for frontend (probably will use in feature)

