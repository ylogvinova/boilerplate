import express from 'express';
import path from "path";
import bodyParser from 'body-parser';
import userRouter from './user/router'
import { verification } from './middlewares/verification'
import { errorHandler } from './middlewares/errors'
import { loggerMiddleware } from './middlewares/logger'

const app = express();

// Configure Express to use EJS (FOR FEATURE)
app.set( "views", path.join( __dirname, "/views" ) );
app.set( "view engine", "ejs" );

app.use(express.static('static'));
app.use(express.static('logs'));

app.use(loggerMiddleware);

app.use(verification);

// parse requests of content-type - application/json
app.use(bodyParser.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// define a route handler for the USER
app.use('/user', userRouter);

// define a route handler for the default home page
app.get( "/", ( req, res ) => {
  // render the index template
  res.render( "index" );
});

// define a route handler for the default home page
app.get( "/protected", ( req, res ) => {
  // render the index template
  res.render( "index" );
});

// Handle errors
app.use(errorHandler);

export default app;
