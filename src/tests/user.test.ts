const db = require ("../../db/models");
const faker = require("faker");
const { userTest, loginUsers, registerUsers, getUsers } = require("../user/controller")

describe("USERS ===>", () => {
  // // Set the db object to a variable which can be accessed throughout the whole test file
  // let thisDb = db;
  //
  // // Before any tests run, clear the DB and run migrations with Sequelize sync()
  // beforeAll(async () => {
  //   await thisDb.sequelize.sync({ force: true })
  // })

  it('Testing to see if Jest works', () => {
    expect(1).toBe(1)
  })

  it('Testing user number', async () => {
    const newNumber = await userTest(3)
    expect(newNumber).toBe(9)
  })

  // it("register and login user", async () => {
  //   const randomString = faker.random.alphaNumeric(10)
  //   const email = `${randomString}@email.com`
  //   const password = `password`
  //
  //   const testUser = {
  //     firstName: randomString,
  //     lastName: randomString,
  //     email: email,
  //     password: password
  //   }
  //
  //   await registerUsers(testUser)
  //
  //   const loggedToken = await loginUsers(email, password)
  //
  //   expect(loggedToken).toBeDefined()
  // })
  //
  // it("Should have an user", async () => {
  //   const users = await getUsers()
  //   expect(users.length).toBe(1)
  // })
  //
  // // After all tests have finished, close the DB connection
  // afterAll(async () => {
  //   await thisDb.sequelize.close()
  // })
})
