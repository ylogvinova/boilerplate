import express from "express";
const jwt = require('jsonwebtoken');
const { secret } = require('../../config/server');

export function verification(req: express.Request, res: express.Response, next: express.NextFunction) {
  const token = req.headers.authorization;
  const allowPath = ['/','/user','/user/login', '/user/register']

  if (token) {
    jwt.verify(token, secret, (err: any, user: any) => {
      if (err) {
        return res.sendStatus(403);
      }
      next();
    });
  }
  if (allowPath.includes(req.url)) {
    next();
  }
  else {
    res.sendStatus(401);
  }
}
