import express from "express";
const path = require('path')
const fs = require('fs')
let now = new Date();
let logfile_name = 'log-' + now.getFullYear() + "-"+ now.getMonth() + "-" + now.getDate() +'.txt'

function writeLogs(fileName: string, data: string) {
  fs.writeFile(fileName, data, { flag: 'a+' }, (err: any) => {
    if (err) throw new Error('Ooops, something went wrong');
  })
}

export function loggerMiddleware(req: express.Request, res: express.Response, next: express.NextFunction) {
  const logInfo = `${req.method} ${req.path} - ${now.getHours()}:${now.getMinutes()}\n`
  const filePath = path.join(__dirname, '../logs/' + logfile_name)
  writeLogs(filePath, logInfo)
  next();
}
