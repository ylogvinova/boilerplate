#Image specification
FROM node:14

#Create working directory
WORKDIR /usr/app


COPY package.json .

RUN npm install && npm i ts-node && npm i nodemon

COPY . .

CMD ["./start.sh"]
